#!/usr/bin/env python
"""
anim_tools - tools to prepare drawings for export as animation

Copyright (C) 2016-2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# standard library
from copy import deepcopy
import sys
import locale

# local library
import inkex
import simpletransform
import cubicsuperpath
from pathmodifier import zSort


# globals
ZFILL = 5

# Possible workaround for encoding issues on Windows
# (see also bug #1518302)
ENCODING = sys.stdin.encoding
if ENCODING == 'cp0' or ENCODING is None:
    ENCODING = locale.getpreferredencoding()


def is_group(node):
    """Check whether node is SVG group element."""
    return node.tag == inkex.addNS('g', 'svg')


def is_layer(node):
    """Check whether node is Inkscape layer."""
    return (node.tag == inkex.addNS('g', 'svg') and
            node.get(inkex.addNS('groupmode', 'inkscape')) == 'layer')


def layers(document, rev=False):
    """Iterate through layers in document, return label and node."""
    for node in document.getroot().iterchildren(reversed=rev):
        if is_layer(node):
            label = node.get(inkex.addNS('label', 'inkscape'), None)
            if sys.version_info < (3,):
                labelstring = label.encode(ENCODING)
            else:
                labelstring = label
            yield (labelstring, node)


def layer_objs(document, rev=False):
    """Iterate through layers in document, return layer node."""
    for node in document.getroot().iterchildren(reversed=rev):
        if is_layer(node):
            yield node


def sublayer_objs(layer, rev=False):
    """Iterate through sublayers of layer, return sub-layer node."""
    for node in layer.iterchildren(reversed=rev):
        if is_layer(node):
            yield node


def get_toplevel_layer(node):
    """Return parent top-level layer of node."""
    if is_layer(node.getparent()):
        node = get_toplevel_layer(node.getparent())
    return node


def get_parent_layer(node):
    """Return parent layer of node."""
    if not is_layer(node.getparent()):
        node = get_parent_layer(node.getparent())
    return node.getparent()


def get_prev_layer(node):
    """Return next layer on same level."""
    for sibling in node.itersiblings(tag=inkex.addNS('g', 'svg'),
                                     preceding=True):
        if is_layer(sibling):
            return sibling


def get_next_layer(node):
    """Return next layer on same level."""
    for sibling in node.itersiblings(tag=inkex.addNS('g', 'svg'),
                                     preceding=False):
        if is_layer(sibling):
            return sibling


def wrap_in_group(node):
    """Wrap node in group, return group."""
    group = inkex.etree.Element(inkex.addNS('g', 'svg'))
    index = node.getparent().index(node)
    node.getparent().insert(index, group)
    group.append(node)
    return group


def group_to_layer(node):
    """Add inkscape groupmode attribute and set it to 'layer'."""
    if is_group(node):
        node.set(inkex.addNS('groupmode', 'inkscape'), 'layer')


def layer_to_group(node):
    """Remove inkscape groupmode attribute if present."""
    if is_layer(node):
        del node.attrib[inkex.addNS('groupmode', 'inkscape')]


# ----- import from path_lib.transform

IDENT_MAT = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]


def invertTransform(mat):
    """Return inverted mat."""
    # pylint: disable=invalid-name
    det = mat[0][0]*mat[1][1] - mat[0][1]*mat[1][0]
    if det != 0:  # det is 0 only in case of 0 scaling
        # invert the rotation/scaling part
        a11 = mat[1][1]/det
        a12 = -mat[0][1]/det
        a21 = -mat[1][0]/det
        a22 = mat[0][0]/det
        # invert the translational part
        a13 = -(a11*mat[0][2] + a12*mat[1][2])
        a23 = -(a21*mat[0][2] + a22*mat[1][2])
        return [[a11, a12, a13], [a21, a22, a23]]
    else:
        return [[0, 0, -mat[0][2]], [0, 0, -mat[1][2]]]


def mat_invert(mat):
    """Return inverted transformation matrix."""
    if hasattr(simpletransform, 'invertTransform'):
        return simpletransform.invertTransform(mat)
    else:
        return invertTransform(mat)


def mat_compose_doublemat(mat1, mat2):
    """Compose two mats into a single one, return mat."""
    return simpletransform.composeTransform(mat1, mat2)


def mat_apply_to(mat, obj):
    """Call applyTransformTo{Path,Node} depending on obj type."""
    # pylint: disable=protected-access
    if isinstance(obj, list):
        simpletransform.applyTransformToPath(mat, obj)
    elif isinstance(obj, inkex.etree._Element):
        simpletransform.applyTransformToNode(mat, obj)


def mat_copy_from(node):
    """Return transformation matrix for node's preserved transform."""
    return simpletransform.parseTransform(node.get('transform'))


def mat_absolute(node):
    """Return transformation matrix transforming node to SVGRoot."""
    if node.getparent() is not None:
        return simpletransform.composeParents(node, IDENT_MAT)
    else:
        return mat_copy_from(node)


def mat_apply_copy_from(source, target):
    """Apply preserved transform from source to target."""
    mat = mat_copy_from(source)
    mat_apply_to(mat, target)


def mat_absolute_diff(node1, node2):
    """Return transformation matrix to transform node2 to node1 coords."""
    mat1 = mat_absolute(node1.getparent())
    mat2 = mat_absolute(node2.getparent())
    return mat_compose_doublemat(mat_invert(mat1), mat2)


def mat_apply_absolute_diff(node1, node2):
    """Transform node2 to node1 coords."""
    mat = mat_absolute_diff(node1, node2)
    mat_apply_to(mat, node2)

# ----- end import from path_lib.geom


def get_element_by_id_from_doc(id_, document):
    """Return element by id from document (XML tree) passed as argument."""
    path = '//*[@id="{0}"]'.format(id_)
    el_list = document.xpath(path, namespaces=inkex.NSS)
    if el_list:
        return el_list[0]
    else:
        return None


def ref_node(doc, node):
    """Return element linked to via href (e.g. by <use> elements)."""
    ref_id = node.get(inkex.addNS('href', 'xlink'))
    if ref_id:
        return get_element_by_id_from_doc(ref_id[1:], doc)
    else:
        return None


def resolve_use(doc, node):
    """Recursively process <use> elements and convert into groups."""
    group = inkex.etree.Element(inkex.addNS('g', 'svg'))
    if node.get('transform'):
        mat = simpletransform.parseTransform(node.get('transform'))
        simpletransform.applyTransformToNode(mat, group)
    mat = simpletransform.parseTransform(
        'translate({0},{1})'.format(node.get('x', 0), node.get('y', 0)))
    simpletransform.applyTransformToNode(mat, group)
    node_orig = ref_node(doc, node)
    if node_orig is not None:
        if node_orig.tag == inkex.addNS('use', 'svg'):
            use_group = resolve_use(doc, node_orig)
            group.append(use_group)
        elif node_orig.tag == inkex.addNS('g', 'svg'):
            sub_group = resolve_group(doc, node_orig)
            group.append(sub_group)
        else:
            group.append(deepcopy(node_orig))
    else:
        inkex.errormsg('Orphaned clone ({0}) ignored.'.format(node.get('id')))
    return group


def resolve_group(doc, node):
    """Recursively process groups, follow links of <use> childs."""
    group = inkex.etree.Element(inkex.addNS('g', 'svg'))
    if node.get('transform'):
        group.set('transform', node.get('transform'))
    for child in node:
        if child.tag == inkex.addNS('use', 'svg'):
            use_group = resolve_use(doc, child)
            group.append(use_group)
        elif child.tag == inkex.addNS('g', 'svg'):
            sub_group = resolve_group(doc, child)
            group.append(sub_group)
        else:
            group.append(deepcopy(child))
    return group


def copy_group_unlink_recursively(group):
    """Return new group with copy of the <g>'s content."""
    doc = group.getroottree()
    new_group = inkex.etree.Element(inkex.addNS('g', 'svg'))
    if group.get('transform'):
        mat = simpletransform.parseTransform(group.get('transform'))
        simpletransform.applyTransformToNode(mat, new_group)
    if len(group) > 0:
        for child in group:
            if child.tag == inkex.addNS('use', 'svg'):
                use_group = resolve_use(doc, child)
                new_group.append(use_group)
            elif child.tag == inkex.addNS('g', 'svg'):
                sub_group = resolve_group(doc, child)
                new_group.append(sub_group)
            else:  # if child.tag == inkex.addNS('path', 'svg'):
                new_group.append(deepcopy(child))
    index = group.getparent().index(group)
    group.getparent().insert(index+1, new_group)
    return new_group


def ungroup_path(group):
    """Todo."""
    if group.get('transform'):
        mat = simpletransform.parseTransform(group.get('transform'))
    else:
        mat = IDENT_MAT
    group_csp = cspp = []
    for child in group:
        if child.tag == inkex.addNS('desc', 'svg'):
            pass
        elif child.tag == inkex.addNS('g', 'svg'):
            cspp = ungroup_path(child)
            simpletransform.applyTransformToPath(mat, cspp)
        elif child.tag == inkex.addNS('path', 'svg'):
            if child.get('d'):
                cspp = cubicsuperpath.parsePath(child.get('d'))
                simpletransform.applyTransformToPath(mat, cspp)
        if len(cspp):
            for subp in cspp:
                group_csp.append(subp)
    return group_csp


def ungroup_combine(group):
    """Todo."""
    group_csp = ungroup_path(group)
    group_path = inkex.etree.Element(inkex.addNS('path', 'svg'))
    group_path.set('d', cubicsuperpath.formatPath(group_csp))
    index = group.getparent().index(group)
    group.getparent().insert(index+1, group_path)
    return group_path


def zip_two_groups(group1, group2, offset=0):
    """Insert (zip) children of group2 into group1."""
    if is_group(group1) and is_group(group2):
        len_group1 = len(group1)
        if offset > 0:
            offset = min(offset, len_group1)
        elif offset < 0:
            offset = max(offset, -1 * len(group2))
        for i, node in enumerate(group2):
            mat_apply_absolute_diff(group1[0], node)
            if offset + i < 0:
                group1.insert(i, node)
            elif offset + i <= len_group1:
                group1.insert(offset + 2*i + 1, node)
            else:
                group1.append(node)
    group2.getparent().remove(group2)


def copy_node_into_group(source, target, merge=False):
    """Helper function for zipping/merging into subgroups."""
    new_node = deepcopy(source)
    mat_apply_to(mat_absolute_diff(target, source), new_node)
    if merge:
        mat_apply_to(mat_invert(mat_copy_from(target)), new_node)
    target.append(new_node)


def copy_node_or_group(source, target, merge=False):
    """Check type and recursively copy content if source is container."""
    if not is_group(source):
        copy_node_into_group(source, target, merge)
    else:
        for child in source:
            copy_node_into_group(child, target, merge)


def zip_into_subgroups(group1, group2, offset=0):
    """Merge children of group1 and group2 into subgroup in new group."""
    if is_group(group1) and is_group(group2):
        parent_group = inkex.etree.Element(inkex.addNS('g', 'svg'))
        group2.getparent().append(parent_group)
        # TODO: should larger offsets be allowed to create 'gaps'?
        if offset >= 0:
            offset = min(offset, len(group1))
            max_count = max(len(group1), offset + len(group2))
        else:
            offset = max(offset, -len(group2))
            max_count = -offset + max(len(group1), offset + len(group2))
        for i in range(max_count):
            zip_group = inkex.etree.Element(inkex.addNS('g', 'svg'))
            parent_group.append(zip_group)
            j = offset + i if offset < 0 else i
            if j < 0:  # prepend from group2
                copy_node_or_group(group2[i], zip_group)
            else:
                if j + 1 <= len(group1):  # copy from group1
                    copy_node_or_group(group1[j], zip_group)
                if i >= offset and j - offset < len(group2):  # merge group2
                    copy_node_or_group(group2[j - offset], zip_group)
            zip_group.set(
                inkex.addNS('label', 'inkscape'), str(i).zfill(ZFILL))
        return parent_group


def merge_into_subgroups(group1, group2, offset=0, keep=False):
    """Merge children of group2 into subgroups of group1."""
    merge = True
    if is_group(group1) and is_group(group2):
        # TODO: should larger offsets be allowed to create 'gaps'?
        if offset >= 0:
            offset = min(offset, len(group1))
            max_count = max(len(group1), offset + len(group2))
        else:
            offset = max(offset, -len(group2))
            max_count = -offset + max(len(group1), offset + len(group2))
        for i in range(max_count):
            j = offset + i if offset < 0 else i
            if j < 0:  # prepend from group2
                merge_group = inkex.etree.Element(inkex.addNS('g', 'svg'))
                group1.insert(i, merge_group)
                copy_node_or_group(group2[i], merge_group, merge)
            else:
                if j + 1 <= len(group1):  # wrap from group1
                    if not is_group(group1[i]):
                        merge_group = wrap_in_group(group1[i])
                    else:
                        merge_group = group1[i]
                else:  # append empty merge group
                    merge_group = inkex.etree.Element(inkex.addNS('g', 'svg'))
                    group1.append(merge_group)
                if i >= offset and j - offset < len(group2):  # merge group2
                    copy_node_or_group(group2[j - offset], merge_group, merge)
            merge_group.set(
                inkex.addNS('label', 'inkscape'), str(i).zfill(ZFILL))
        # all done, delete merged group
        if not keep:
            group2.getparent().remove(group2)


class AnimTools(inkex.Effect):
    """Tools to manipulate symbol and icon sets."""

    def __init__(self):
        """Todo."""
        inkex.Effect.__init__(self)

        # instance attributes
        self.objects = []
        self.commonlayers = []

        # groups
        self.OptionParser.add_option("--action_groups",
                                     action="store",
                                     type="string",
                                     dest="action_groups",
                                     default="object_to_layer",
                                     help="")
        self.OptionParser.add_option("--use_offset",
                                     action="store",
                                     type="inkbool",
                                     dest="use_offset",
                                     default=False,
                                     help="")
        self.OptionParser.add_option("--offset_groups",
                                     action="store",
                                     type="int",
                                     dest="offset_groups",
                                     default=0,
                                     help="")
        # layers
        self.OptionParser.add_option("--action_layers",
                                     action="store",
                                     type="string",
                                     dest="action_layers",
                                     default="layer_to_group",
                                     help="")
        # misc
        self.OptionParser.add_option("--action_misc",
                                     action="store",
                                     type="string",
                                     dest="action_misc",
                                     default="ungroup_keep_id",
                                     help="")
        # tabs
        self.OptionParser.add_option("--nb_main",
                                     action="store",
                                     type="string",
                                     dest="nb_main",
                                     help="")

    def convert_layers_to_groups(self):
        """Convert selected layers to groups."""
        for node in self.objects:
            layer_to_group(node)

    def convert_groups_to_layers(self):
        """Convert selected groups to layers."""
        for node in reversed(self.objects):
            parent_layer = node.getparent()
            if parent_layer.getparent() is not None:
                index = parent_layer.getparent().index(parent_layer)
                parent_layer.getparent().insert(index+1, node)
                mat_apply_copy_from(parent_layer, node)
            group_to_layer(node)

    def convert_groups_to_sublayers(self):
        """Convert selected groups to layers."""
        for node in self.objects:
            group_to_layer(node)

    def unwrap_sublayers_in_layer(self):
        """Move objects from sub-layer to parent, delete sub-layer."""
        for node in self.objects:
            if is_layer(node):
                parent = node.getparent()
                index = parent.index(node)
                for child in node.iterchildren():
                    mat_apply_copy_from(node, child)
                    parent.insert(index, child)
                    index += 1
                parent.remove(node)

    def wrap_objects_in_sublayers(self):
        """Wrap each selected object into new sub-layer of parent."""
        for i, node in enumerate(self.objects):
            node_parent = node.getparent()
            if not is_layer(node_parent):
                group_to_layer(node_parent)
                node_parent.set(
                    inkex.addNS('label', 'inkscape'), "anim")
            new_group = wrap_in_group(node)
            new_group.set(
                inkex.addNS('label', 'inkscape'), str(i).zfill(ZFILL))
            group_to_layer(new_group)

    def wrap_objects_in_groups(self):
        """Wrap each selected object in a group."""
        for i, node in enumerate(self.objects):
            new_group = wrap_in_group(node)
            new_group.set(
                inkex.addNS('label', 'inkscape'), str(i).zfill(ZFILL))

    def zip_objects_into_subgroups(self):
        """Wrapper to process more than 2 selected groups for zipping."""
        offset = self.options.offset_groups
        zgroup = zip_into_subgroups(self.objects[0], self.objects[1], offset)
        if len(self.objects) > 2:
            for node in self.objects[2:]:
                if offset >= 0:
                    offset += self.options.offset_groups
                merge_into_subgroups(zgroup, node, offset, keep=True)

    def merge_objects_into_subgroups(self):
        """Wrapper to process more than 2 selected groups for merging."""
        offset = self.options.offset_groups
        for node in self.objects[1:]:
            merge_into_subgroups(self.objects[0], node, offset)
            if offset >= 0:
                offset += self.options.offset_groups

    def ungroup_selection(self, mode="keep_id"):
        """Todo."""
        for node in self.objects:
            if len(node) == 1:
                index = node.getparent().index(node)
                child = node[0]
                node.getparent().insert(index, child)
                if mode == 'keep_id':
                    child.set('id', node.get('id'))
                if 'transform' in node.attrib:
                    child.attrib['transform'] = node.attrib['transform']
                node.getparent().remove(node)

    def get_selection(self, mode="object"):
        """Fill instance attribute (list) with refs to target objects."""
        # pylint: disable=too-many-branches
        if mode == "object":
            for id_ in zSort(self.document.getroot(), self.selected.keys()):
                self.objects.append(self.selected[id_])
        elif mode == "group":
            for id_ in zSort(self.document.getroot(), self.selected.keys()):
                if is_group(self.selected[id_]):
                    self.objects.append(self.selected[id_])
        elif mode == "groupcontent":
            for id_ in zSort(self.document.getroot(), self.selected.keys()):
                if is_group(self.selected[id_]):
                    for child in self.selected[id_].iterchildren():
                        self.objects.append(child)
        elif mode == "rootlayers":
            for node in sublayer_objs(self.document.getroot()):
                self.objects.append(node)
        elif mode == "current_layer":
            self.objects.append(self.current_layer)
        elif mode == "sublayers":
            for node in sublayer_objs(self.current_layer):
                self.objects.append(node)
        else:
            pass

    def page_misc(self):
        """Todo."""
        if self.options.action_misc == "ungroup_keep_id":
            self.get_selection(mode="group")
            self.ungroup_selection(mode="keep_id")
        elif self.options.action_misc == "ungroup_combine":
            self.get_selection(mode="group")
            for group in self.objects:
                ungroup_combine(group)
        elif self.options.action_misc == "unlink_recursively":
            self.get_selection(mode="group")
            for group in self.objects:
                copy_group_unlink_recursively(group)
        else:
            pass

    def page_layers(self):
        """Dispatch command for chosen action in layers tab."""
        if self.options.action_layers == "object_to_layer":
            self.get_selection(mode="object")
            self.wrap_objects_in_sublayers()
        elif self.options.action_layers == "group_to_sublayer":
            self.get_selection(mode="group")
            self.convert_groups_to_sublayers()
        elif self.options.action_layers == "groupcontent_to_layer":
            self.get_selection(mode="groupcontent")
            self.wrap_objects_in_sublayers()
        if self.options.action_layers == "layer_to_group":
            self.get_selection(mode="current_layer")
            self.convert_layers_to_groups()
        elif self.options.action_layers == "layers_to_group":
            self.get_selection(mode="rootlayers")
            self.convert_layers_to_groups()
        if self.options.action_layers == "group_to_layer":
            self.get_selection(mode="group")
            self.convert_groups_to_layers()
        elif self.options.action_layers == "sublayer_to_group":
            self.get_selection(mode="sublayers")
            self.convert_layers_to_groups()
        elif self.options.action_layers == "unwrap_sublayer":
            self.get_selection(mode="sublayers")
            self.unwrap_sublayers_in_layer()
        else:
            pass

    def page_groups(self):
        """Dispatch command for chosen action in groups tab."""
        if not self.options.use_offset:
            self.options.offset_groups = 0
        if self.options.action_groups == "object_to_group":
            self.get_selection(mode="object")
            self.wrap_objects_in_groups()
        elif self.options.action_groups == "zip_groups":
            self.get_selection(mode="group")
            if len(self.objects) == 2:
                offset = self.options.offset_groups
                zip_two_groups(self.objects[0], self.objects[1], offset)
        elif self.options.action_groups == "zip_into_subgroups":
            self.get_selection(mode="group")
            if len(self.objects) >= 2:
                self.zip_objects_into_subgroups()
        elif self.options.action_groups == "merge_into_subgroups":
            self.get_selection(mode="group")
            if len(self.objects) >= 2:
                self.merge_objects_into_subgroups()
        else:
            pass

    def effect(self):
        """Run action dispatcher based on current tab in GUI."""
        try:
            page_cmd = getattr(self, self.options.nb_main.strip("\""))
        except AttributeError:
            page_cmd = None
            inkex.errormsg('Unknown tab {}'.format(self.options.nb_main))
        if page_cmd is not None:
            page_cmd()
        else:
            pass


if __name__ == '__main__':
    ME = AnimTools()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
